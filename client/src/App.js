import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import AdChart from './components/AdChart';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AdChart ads={[{id: 1, name: '123'}]}/>
      </div>
    );
  }
}

export default App;
