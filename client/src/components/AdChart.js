import React, { Component } from 'react';
import Client from './Client';
import Column from './Column';

class AdChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ads: []
    };
  }

  fetchAds = (e) => {
    Client.get_ads((ads) => {
      this.setState({
        ads: ads.ads,
      });
    });

    Client.get_ads_metrics((ads_metrics) => {
      var ads_info = {};
      ads_metrics.rows.forEach((row, idx) => (
        ads_info[row.remote_id] = row
      ));
      this.setState({
        column_names: ads_metrics.column_names,
        ads_info: ads_info
      });
    });
  }

  render() {
    const ad_names = this.state.ads.map((elem,idx) => ( elem.name ))
    const ad_remote_ids = this.state.ads.map((elem,idx) => ( elem.remote_id ))

    return (
      <div className="ad-chart-cont">
        AdChart
        <br/>
        <button onClick={this.fetchAds}>
          Get Metrics
        </button>
        <div className='ad-chart'>
          <div id='ad-names'>
            {this.state.ads && this.state.ads.length > 0 &&
              <Column name='Name' row_vals={ad_names} />
            }
          </div>
          <div id='ad-metrics'>
            {this.state.column_names && this.state.ads_info &&
              this.state.column_names.map((col_name, idx) => (
                <Column key={idx} name={col_name} row_vals={ad_remote_ids.map((remote_id, idx) => (this.state.ads_info[remote_id][col_name]))} />
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

export default AdChart;
