import React, { Component } from 'react';

class Column extends Component {
  render() {
    return (
      <ul className='column'>
        <li className='col-header'>{this.props.name}</li>
        {this.props.row_vals.map((value, idx) => (
          <li key={idx}>{value}</li>
        ))}
      </ul>
    )
  }
}

export default Column;
