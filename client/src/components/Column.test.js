import React from 'react';
import Column from './Column';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const column = renderer.create(
    <Column name='Test Column' row_vals={[1,2,3]} />
  ).toJSON();
  expect(column).toMatchSnapshot();
});
